Dear reviewers,

This is the anonymous version of SPT. Some features, e.g. dashboard and reports, are disabled and will be availble in a near future.
SPT is already in a GitHub Education repository that will be used later on to support the dissemination of SPT.

SPT requires Java 8.
payload.csv contains the information for replicating the empirical study.

java -jar spt.jar -h to see the help.

Thank you for your time,

CSE Lab